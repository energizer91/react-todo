/**
 * Created by energizer on 27.10.17.
 */

import React from 'react';
import PropTypes from 'prop-types';
import './style.css'

const TodoItem = ({text, done, onDelete}) => {
    return (
        <li className="todoItem">
            <div className="left">
                <span className="done" />
                <span className="text">{text}</span>
            </div>
            <div className="delete" onClick={onDelete} />
        </li>
    )
};

TodoItem.propTypes = {
    text: PropTypes.string.isRequired,
    done: PropTypes.bool.isRequired,
    onDelete: PropTypes.func.isRequired
};

export default TodoItem;