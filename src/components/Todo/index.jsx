/**
 * Created by energizer on 27.10.17.
 */

import React from 'react';
import PropTypes from 'prop-types';
import TodoItem from '../TodoItem';
import './style.css';

export default class Todo extends React.Component {
    static propTypes = {
        todos: PropTypes.arrayOf({
            text: PropTypes.string,
            done: PropTypes.bool
        }).isRequired
    };

    onDelete(index) {

    }

    render() {
        const {todos} = this.props;
        return (
            <ul className="todo">
                {todos.map((todo, index) => (
                    <TodoItem
                        key={index}
                        text={todo.text}
                        done={todo.done}
                        onDelete={this.onDelete.bind(this, index)}
                    />
                ))}
            </ul>
        )
    }
}

