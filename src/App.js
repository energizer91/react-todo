import React from 'react';
import Todo from './components/Todo';
import './App.css';

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            todos: [
                {
                    text: 'Building products',
                    done: false
                },
                {
                    text: 'Mobile App Polish',
                    done: true
                },
                {
                    text: 'Couponing feature extension',
                    done: false
                }
            ]
        }
    }

    render() {
        return (
            <div className="App">
                <div className="header">
                    Task Manager
                </div>
                <Todo todos={this.state.todos}/>
            </div>
        );
    }
}

export default App;
